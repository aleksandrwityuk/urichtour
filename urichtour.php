<?php
/*
Plugin Name: Urich Tours
Plugin URI: https://tours.com/
Description: Tour booking plugin.
Version: 1.0
Author: Alexander Wityuk
Author URI: 
License: 
Text Domain: 
*/

if ( !function_exists( 'add_action' ) ) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}

define( 'TOUR__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

require_once( TOUR__PLUGIN_DIR . 'tour-class.php' );

add_action( 'init', array( 'Tour', 'register_tour_post_type') );
add_action('save_post', array( 'Tour', 'add_country_save_data') );
add_action('save_post', array( 'Tour', 'add_tour_date_save_data') );
add_action('save_post', array( 'Tour', 'add_tour_price_save_data') );
add_action( 'add_meta_boxes', array( 'Tour', 'add_meta_box' ) );
add_filter('manage_tour_posts_columns', array('Tour','filter_urichtour_columns'));
add_action( 'manage_tour_posts_custom_column',array ('Tour','action_custom_columns_content'), 10, 2 );