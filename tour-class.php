<?php

/**
 * 
 */
class Tour
{
    private static $mnths = array(
                        'Австралия', 
                        'Германия',
                        'Италия',
                        'Канада'
                        );

    public static function add_meta_box( $post_type ){

        add_meta_box( 'countries', 'Страны', array( 'Tour', 'add_country_box_field'), 'tour', 'normal', 'high' );
        add_meta_box( 'date-tour', 'Дата тура', array( 'Tour','tour_date_box_field'), 'tour', 'side', 'low' );
        add_meta_box( 'price-tour', 'Цена:', array( 'Tour','tour_price_box_field'), 'tour', 'side', 'low' );
    }

    public static function register_tour_post_type() {

        /*----------------------------------------------------------------------------------------------------*/
        /*  Create the post_type called "Тур" 
        /*----------------------------------------------------------------------------------------------------*/

        $args = array(
                'public' => true,
                'label'  => 'Туры',
                'public'             => true,
                'publicly_queryable' => true,
                'show_ui'            => true,
                'show_in_menu'       => true,
                'query_var'          => true,
                'rewrite'            => array( 'slug' => 'tour' ),
                'capability_type'    => 'post',
                'has_archive'        => true,
                'hierarchical'       => false,
                'menu_position'      => null,
                'supports'           => array( 'title', 'editor', 'thumbnail')
        );
        register_post_type( 'tour', $args );

    }
    /*
    * Counties
    */
    public static function add_country_box_field ($post) {
        wp_nonce_field( basename(__FILE__), 'country_mam_nonce' );
        $country = maybe_unserialize( get_post_meta( $post->ID, 'country-tour', true ) );
        ?>
        <style type="text/css">
        /*#select_media_file_event .inside {
            overflow: hidden;
        }*/
        </style>
        <select name="country" id="country" class="testo_modulo" style="font-size:12px;">
            <?php for ($i=0; $i < count(self::$mnths); $i++) :?>
                    <option <?php selected( $country, $i ); ?> value="<?php echo $i; ?>"><?php echo self::$mnths[$i]; ?></option>
            <?php endfor; ?>
        </select>
        <?php
    }
    public static function add_country_save_data ($post_id) {
        $is_autosave = wp_is_post_autosave( $post_id );
        $is_revision = wp_is_post_revision( $post_id );
        $is_valid_nonce = ( isset( $_POST[ 'country_mam_nonce' ] ) && wp_verify_nonce( $_POST[ 'country_mam_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';

        if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
            return;
        }

        if ( ! empty( $_POST['country'] ) ) {
            update_post_meta( $post_id, 'country-tour', $_POST['country'] );

        } else {
            delete_post_meta( $post_id, 'country-tour' );

        }

    }

    /*
    * Date tour
    */

    public static function tour_date_box_field ($post) {
        wp_nonce_field( basename(__FILE__), 'tour_date_mam_nonce' );
        $date_start = maybe_unserialize( get_post_meta( $post->ID, 'date-start-tour', true ) );
        $date_finish = maybe_unserialize( get_post_meta( $post->ID, 'date-finish-tour', true ) );
        ?>
        <label for="date-start">Начало тура:</label>
        <input type="date" id="date-start" name="date-start" value="<?php echo $date_start; ?>">
        <label for="date-finish">Конец тура:</label>
        <input type="date" id="date-finish" name="date-finish" value="<?php echo $date_finish; ?>">
        <?php
    }

    public static function add_tour_date_save_data($post_id) {

        $is_autosave = wp_is_post_autosave( $post_id );
        $is_revision = wp_is_post_revision( $post_id );
        $is_valid_nonce = ( isset( $_POST[ 'tour_date_mam_nonce' ] ) && wp_verify_nonce( $_POST[ 'tour_date_mam_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';

        if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
            return;
        }

        if ( ! empty( $_POST['date-start'] ) ) {
            update_post_meta( $post_id, 'date-start-tour', $_POST['date-start'] );
            update_post_meta( $post_id, 'date-finish-tour', $_POST['date-finish'] );
        } else {
            delete_post_meta( $post_id, 'date-start-tour' );
            delete_post_meta( $post_id, 'date-finish-tour' );
        }

    }

    /*
    * Price
    */
    public static function tour_price_box_field ($post) {
        wp_nonce_field( basename(__FILE__), 'tour_price_mam_nonce' );
        $price = maybe_unserialize( get_post_meta( $post->ID, 'price-tour', true ) );
        ?>
        <label for="price">Цена тура:</label>
        <input type="text" id="price" name="price" value="<?php echo $price; ?>">
        <?php
    }


    public static function add_tour_price_save_data ($post_id) {

        $is_autosave = wp_is_post_autosave( $post_id );
        $is_revision = wp_is_post_revision( $post_id );
        $is_valid_nonce = ( isset( $_POST[ 'tour_price_mam_nonce' ] ) && wp_verify_nonce( $_POST[ 'tour_price_mam_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';

        if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
            return;
        }

        if ( ! empty( $_POST['price'] ) ) {
            update_post_meta( $post_id, 'price-tour', $_POST['price'] );
        } else {
            delete_post_meta( $post_id, 'price-tour' );
        }

    }

    /*
    *  add columns to custom post tables "tour"
    *
    */

    public static function filter_urichtour_columns( $columns ) {

        $columns['country-tour'] = 'Страна';
        $columns['date-tour'] = 'Дата';
        $columns['price-tour'] = 'Стоимость';


        return $columns;
    }
    
    public static function action_custom_columns_content ( $column_id, $post_id ) {
        //run a switch statement for all of the custom columns created
        switch( $column_id ) { 
            case 'country-tour':
                $value = get_post_meta( $post_id, 'country-tour', true );
                echo self::$mnths[$value];
            break;
            case 'date-tour':
                echo ($value = get_post_meta( $post_id, 'date-start-tour', true ) ) ? $value : '';
                echo " - ";
                echo ($value = get_post_meta( $post_id, 'date-finish-tour', true ) ) ? $value : '';
            break;
            case 'price-tour':
                echo ($value = get_post_meta( $post_id, 'price-tour', true ) ) ? $value : '';
            break;

       }
    }
}